This is an assignment for Noroff.

The application is a program that takes in a word input and translates it into sign language.

### Running the application

**Server**

To run the application you need to first start json server.
Open a terminal in the folder lost-in-translation-server and write the commando

```
npm start
```

**Client**

After the server is running you can open the folder client in a terminal and write the commando

```
npm start
```

### Assignment requirements

**Features**

- [x] React frameworks
- [x] React router
- [x] Json database with json-server

**Optional features**

- [x] React redux with redux Toolkit
- [x] Auth0 for authentication

**Startup page**

- [x] Should see a "Login page", able to enter their name
      **(done with Auth0 so only able to login and signup with email)**
- [x] Store in node.js database and then display the main page, translation page
- [x] Session management solution, logged in users redirected to the translation page. can use browser local storage. NEVER STORE ID'S IN LOCAL STORAGE

**Translation page**

- [x] Only visible if they are logged in, redirect back to login page if not logged in.
- [x] Must click on translate button to the right of the input to trigger the translation.
- [x] Translations must be stored in node.js database solution
- [x] Sign language characters must appear in the "translated" box.

**Profile page**

- [x] The profile page must display the last 10 translations for the current user
- [x] Button to clear the translations
- [x] Logout should clear all the storage and return to start page
