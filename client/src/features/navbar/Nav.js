import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { Link } from "react-router-dom";
import Login from "../authentication/Login";
import LogoutButton from "../authentication/LogoutButton";
import styles from "./Nav.module.css";
import Icon from "@mdi/react";
import { mdiAccountCircle } from "@mdi/js";

function Nav() {
  const { isAuthenticated, user } = useAuth0();
  return (
    <nav className={styles.navBarContainer}>
      <ul className={styles.nav}>
        {isAuthenticated && (
          <>
            <img
              src="../../../../Logo.png"
              alt="logo"
              className={styles.logoImg}
            ></img>
            <li className={styles.navBarHome}>
              <Link to="/">Lost in Translation</Link>
            </li>
          </>
        )}
        <li className={styles.navBarLogin}>
          <Login></Login>
          <LogoutButton></LogoutButton>
        </li>

        {isAuthenticated && (
          <li className={styles.navBarProfile}>
            <p className={styles.userEmail}>{user.email}</p>
            <Link to="/profile">
              <Icon
                path={mdiAccountCircle}
                className={styles.profileIcon}
                size={2}
              ></Icon>
            </Link>
          </li>
        )}
      </ul>
    </nav>
  );
}
export default Nav;
