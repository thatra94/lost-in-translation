import { useSelector } from "react-redux";

import { selectTranslations } from "../translation/translationSlice";
import { getTranslationImages } from "../translation/TranslationAPI";
import styles from "./Profile.module.css";
import { generateId } from "../../utils/generateId";
export const ProfileLastTranslations = () => {
  const lastTranslationsList = useSelector(selectTranslations);

  console.log(lastTranslationsList);

  let listTranslations = [];
  let list = [];

  //Get translation for each word
  for (let i = 0; i < lastTranslationsList.length; i++) {
    let translationObject = {
      word: lastTranslationsList[i],
      images: getTranslationImages(lastTranslationsList[i]),
    };
    list.push(translationObject);
  }

  //create list element for each word and sign images
  for (let i = 0; i < lastTranslationsList.length; i++) {
    if (lastTranslationsList.length > 0) {
      listTranslations = list.map((translations) =>
        mapWordsToImages(translations, mapImages)
      );
    }
  }

  function mapWordsToImages(translations, mapImages) {
    return (
      <li key={generateId()} className={styles.listElement}>
        <div className={styles.wordContainer}> {translations.word}</div>

        <div className={styles.imagesContainer}>
          {translations.images.map((image) => mapImages(image, translations))}
        </div>
      </li>
    );
  }

  function mapImages(image, translations) {
    return (
      <img
        key={generateId()}
        className={styles.lastTranslationsImages}
        src={image}
        alt={translations.word}
      />
    );
  }

  return (
    <div className={styles.test}>
      <div className={styles.listContainer}>
        <ul className={styles.lastTranslationList}>{listTranslations}</ul>
      </div>
    </div>
  );
};

export default ProfileLastTranslations;
