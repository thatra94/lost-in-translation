import { useAuth0 } from "@auth0/auth0-react";
import { ProfileLastTranslations } from "./ProfileLastTranslations";
import { useEffect } from "react";
import { fetchcUserByEmail } from "../translation/translationSlice";
import { useDispatch } from "react-redux";
import { ProfileDeleteTranslations } from "./ProfileDeleteTranslations";

export const Profile = () => {
  const { user, isAuthenticated, isLoading } = useAuth0();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchcUserByEmail(user.email));
  }, [dispatch]);

  console.log(user);
  if (isLoading) {
    return <div>Loading ...</div>;
  }

  return (
    isAuthenticated && (
      <div>
        <img src={user.picture} alt={user.name} />
        <h2>{user.name}</h2>
        <p>{user.email}</p>
        <ProfileDeleteTranslations></ProfileDeleteTranslations>
        <h2>Last translations</h2>
        <ProfileLastTranslations></ProfileLastTranslations>
      </div>
    )
  );
};

export default Profile;
