import { useAuth0 } from "@auth0/auth0-react";
import { useSelector, useDispatch } from "react-redux";
import {
  selectUserId,
  deleteTranslations,
} from "../translation/translationSlice";
import { saveUserTranslations } from "../translation/TranslationAPI";
export const ProfileDeleteTranslations = () => {
  const dispatch = useDispatch();
  const { user } = useAuth0();

  let id = useSelector(selectUserId);

  const handleDeleteTranslationsClick = async () => {
    dispatch(deleteTranslations());
    saveUserTranslations(id, [], user.email);
  };

  return (
    <>
      <button onClick={handleDeleteTranslationsClick}>
        Delete translations
      </button>
    </>
  );
};

export default ProfileDeleteTranslations;
