import { TranslationInput } from "./TranslationInput";
import { useAuth0 } from "@auth0/auth0-react";
import { TranslationOutput } from "./TranslationOutput";
import { useEffect } from "react";
import { fetchcUserByEmail } from "./translationSlice";
import { useSelector, useDispatch } from "react-redux";
import { selectUserId } from "./translationSlice";

export function Translation() {
  const { isAuthenticated, user } = useAuth0();
  let id = useSelector(selectUserId);

  const dispatch = useDispatch();
  useEffect(() => {
    if (isAuthenticated) {
      if (id === null) {
        dispatch(fetchcUserByEmail(user.email));
      }
    }
  });

  return (
    <>
      {!isAuthenticated && (
        <>
          <img src="../../../../Logo-Hello.png" alt="logo"></img>
          <h1>Please log in to begin translating</h1>
        </>
      )}
      {isAuthenticated && (
        <>
          <TranslationInput></TranslationInput>
          <TranslationOutput></TranslationOutput>
        </>
      )}
    </>
  );
}

export default Translation;
