import { useSelector } from "react-redux";
import { selectInput } from "./translationSlice";
import { getTranslationImages } from "./TranslationAPI";
import styles from "./Translation.module.css";
import { generateId } from "../../utils/generateId";

export function TranslationOutput() {
  const input = useSelector(selectInput);
  let imageUrlArray = getTranslationImages(input);

  return (
    <div className={styles.translationContainer}>
      <div className={styles.signsContainer}>
        {imageUrlArray.map((image) => (
          <img
            className={styles.translationImage}
            key={generateId()}
            src={image}
            alt="signs"
          />
        ))}
      </div>
    </div>
  );
}

export default TranslationOutput;
