const BASE_URL = "http://localhost:5000/";

export function getTranslationImages(sentenceToTranslate) {
  console.log(sentenceToTranslate);

  let imageUrlArray = [];
  for (let i = 0; i < sentenceToTranslate.length; i++) {
    let letter = sentenceToTranslate.charAt(i);
    if (letter !== " ") {
      imageUrlArray.push(getImageUrlArray(letter));
    }
  }
  return imageUrlArray;
}

function getImageUrlArray(letter) {
  return `http://localhost:5000/individual_signs/${letter}.png`;
}

export function saveUserTranslations(id, lastTranslations, email) {
  let userData = {
    email: email,
    translations: lastTranslations,
    id: id,
  };
  if (id === null) {
    postUserTranslations(userData);
  } else {
    putUserTranslations(id, userData);
  }
}

function postUserTranslations(userData) {
  fetch(`${BASE_URL}users`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(userData),
  })
    .then((response) => response.json())
    .then((userData) => {
      console.log("Success:", userData);
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

export function putUserTranslations(userId, userData) {
  fetch(`${BASE_URL}users/${userId}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(userData),
  })
    .then((response) => response.json())
    .then((userData) => {
      console.log("Success:", userData);
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

export function fetchUser(email) {
  return fetch(`${BASE_URL}users?email=${email}`)
    .then((r) => r.json())
    .then((result) => {
      console.log(result[0]);
      return result[0];
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}
