import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  setInput,
  selectUserId,
  setTranslations,
  selectTranslations,
  fetchcUserByEmail,
} from "./translationSlice";
import { saveUserTranslations } from "./TranslationAPI";
import { useAuth0 } from "@auth0/auth0-react";
import styles from "./Translation.module.css";
import { mdiArrowRightCircle, mdiKeyboardOutline } from "@mdi/js";
import Icon from "@mdi/react";

export function TranslationInput() {
  const { user } = useAuth0();
  const dispatch = useDispatch();
  const [translationText, setTranslationText] = useState("");
  let translations = useSelector(selectTranslations);
  let id = useSelector(selectUserId);

  const onTranslationQueryChanged = (event) => {
    setTranslationText(event.target.value);
  };

  useEffect(() => {
    saveUserTranslations(id, translations, user.email);
  }, [translations]);

  const handleTranslationClick = () => {
    dispatch(setInput(translationText));
    dispatch(setTranslations(translationText));
    if (id === null) {
      dispatch(fetchcUserByEmail(user.email));
    }
  };
  return (
    <div className={styles.container}>
      <form className={styles.searchContainer}>
        <Icon path={mdiKeyboardOutline} className={styles.keyboardIcon}></Icon>
        <input
          className={styles.searchInput}
          type="text"
          placeholder="Enter a text to translate"
          maxLength="40"
          value={translationText}
          onChange={onTranslationQueryChanged}
        />
        <Icon
          path={mdiArrowRightCircle}
          onClick={handleTranslationClick}
          className={styles.searchButton}
        ></Icon>
      </form>
    </div>
  );
}

export default TranslationInput;
