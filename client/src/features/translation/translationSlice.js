import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { fetchUser } from "./TranslationAPI";

export const fetchcUserByEmail = createAsyncThunk(
  "translations/fetchcUserId",
  async (email) => {
    return fetchUser(email);
  }
);

export const translationSlice = createSlice({
  name: "translation",
  initialState: {
    inputValue: 0,
    user: {
      email: "",
      id: null,
      translations: [],
    },
  },
  reducers: {
    setInput: (state, action) => {
      state.inputValue = action.payload;
    },
    setTranslations: (state, action) => {
      if (state.user.translations.length >= 10) {
        state.user.translations.shift();
      }
      state.user.translations.push(action.payload);
    },
    deleteTranslations: (state) => {
      state.user.translations = [];
    },
  },
  extraReducers: {
    [fetchcUserByEmail.pending]: (state, action) => {
      state.userStatus = "loading";
    },
    [fetchcUserByEmail.fulfilled]: (state, { payload }) => {
      console.log(payload);
      if (payload) {
        state.user = payload;
        state.userStatus = "success";
      }
    },
    [fetchcUserByEmail.rejected]: (state, action) => {
      state.userStatus = "failed";
    },
  },
});

export const {
  setInput,
  setTranslations,
  deleteTranslations,
} = translationSlice.actions;

export const selectTranslations = (state) =>
  state.translation.user.translations;

export const selectInput = (state) => state.translation.inputValue;
export const selectUserId = (state) => state.translation.user.id;
export default translationSlice.reducer;
