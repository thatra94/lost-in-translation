import React from "react";
import { Profile } from "./features/profile/Profile";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.css";
import Translation from "./features/translation/Translation";
import Nav from "./features/navbar/Nav";
import "./fonts/LoveYaLikeASister-Regular.ttf";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <header className="App-header">
          <Nav></Nav>
        </header>
        <main>
          <Switch>
            <Route exact path="/" component={Translation}></Route>
            <Route exact path="/profile" component={Profile}></Route>
          </Switch>
        </main>
      </div>
    </BrowserRouter>
  );
}

export default App;
