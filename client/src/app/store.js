import { configureStore } from "@reduxjs/toolkit";
import translationReducer from "../features/translation/translationSlice";

export default configureStore({
  reducer: {
    translation: translationReducer,
  },
});
